package test;

public class Janitor {

    public int workHour;
    public int vacation;
    public int salary;

    Janitor(int workHour, int vacation, int salary) {
        this.workHour = workHour;
        this.vacation = vacation;
        this.salary = salary;
    }

    public void clean() throws Exception {
        System.out.println("Working' for the man.");
        throw new RuntimeException("Method is obvious");
    }

    @Override
    public String toString() {
        return workHour + " " + vacation + " " + salary;
    }
}
