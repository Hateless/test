package test;

import java.util.ArrayList;
import java.util.List;

public class Main {

    public static void main(String[] args) throws Exception {
        List<Janitor> jList = new ArrayList<Janitor>();
        listFill(jList);
        listOutput(jList);
        System.out.println("");
        listAddition(jList);
        listOutput(jList);

        Janitor[] jArray = new Janitor[4];
        arrayFill(jArray);
        arrayTransfer(jArray);
        arrayOutput(jArray);


    }

    public static void listFill(List<Janitor> jList) {
        Janitor j1 = new Janitor(1000, 1000, 1000);
        Janitor j2 = new Janitor(2000, 2000, 2000);
        Janitor j3 = new Janitor(3000, 3000, 3000);
        Janitor j4 = new Janitor(4000, 4000, 4000);
        jList.add(j1);
        jList.add(j2);
        jList.add(j3);
        jList.add(j4);
    }

    public static void listAddition(List<Janitor> jList) {
        jList.add(new Janitor(5, 5, 5));
    }

    public static void listOutput(List<Janitor> jList) {
        for (Janitor j : jList) {
            System.out.println(j);
        }
    }

    public static void arrayFill(Janitor[] aList) {
        Janitor j1 = new Janitor(1000, 1000, 1000);
        Janitor j2 = new Janitor(2000, 2000, 2000);
        Janitor j3 = new Janitor(3000, 3000, 3000);
        Janitor j4 = new Janitor(4000, 4000, 4000);
        aList[0] = j1;
        aList[1] = j2;
        aList[2] = j3;
        aList[3] = j4;
    }

    public static void arrayTransfer(Janitor[] j) {
        Janitor temp = j[0];
        j[0] = j[1];
        j[1] = temp;
    }

    public static void arrayOutput(Janitor[] aList) {
        for (Janitor j : aList) {
            System.out.println(j);
        }
    }
}