
package test;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.Serializable;


public class Employee implements Serializable {
    private Janitor janitor;
    public Employee(Janitor janitor){
        this.janitor=janitor;
    }
    public void serial() throws IOException{
    ByteArrayOutputStream stream = new ByteArrayOutputStream();
    ObjectOutputStream in = new ObjectOutputStream(stream);
    in.writeObject(janitor);
    }
}
